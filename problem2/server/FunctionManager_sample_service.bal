import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);

Function[] functions = [
    {
    id: "0",
    name: "Square Root",
    versions: [
    {
        id: "1",
        developer: {
            name: "John Smith",
            email: "johnsmith@gmail.com"
        },
        language: "JavaScript",
        keywords: ["square", "math", "exponentials"],
        contents: "y = x^2"
    }, 
    {
        id: "3",
        developer: {
            name: "Jane Doe",
            email: "janedoe@gmail.com"
        },
        language: "C++",
        keywords: ["square", "math", "exponentials"],
        contents: "y = x ** 2"
    }
    ]
}
];

function findFunctionIndexById(string id) returns int? {
    int? idx = ();
    foreach int i in 0 ... functions.length() - 1 {
        var fn = functions[i];
        if fn.id == id {
            idx = i;
            break;
        }
    }
    return idx;
}

function findFunctionById(string id) returns Function|error {
    int? index = findFunctionIndexById(id);
    if index != () {
        return functions[index];
    } else {
        return error("No function with ID '" + id + "' exists.");
    }
}

function findFunctionVersionIndexById(Function fn, string versionId) returns int? {
    int? idx = ();
    foreach int i in 0 ... fn.versions.length() - 1 {
        var fnVersion = fn.versions[i];
        if fnVersion.id == versionId {
            idx = i;
            break;
        }
    }
    return idx;
}

function findFunctionVersionById(Function fn, string versionId) returns FunctionVersion|error {
    int? index = findFunctionVersionIndexById(fn, versionId);
    if index != () {
        return fn.versions[index];
    } else {
        return error("Function '" + fn.id + "' has no version with ID '" + versionId + "'.");
    }
}

function checkKeywordsMatch(string[] keywords, string[] queryKeywords) returns boolean {
    // Note that we convert the keywords to lowercase to allow matches with different capitilzation.
    string[] lowercaseKeywords = keywords.map(k => k.toLowerAscii());
    // Make sure every `queryKeyword` exists in `keywords`
    foreach string word in queryKeywords {
        boolean exists = lowercaseKeywords.indexOf(word.toLowerAscii()) is int;
        if !exists {
            return false;
        }
    }
    return true;
}

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "FunctionManager" on ep {
    remote function add_new_fn(AddNewFnRequest value) returns GenericResponse|error {
        int added_fn = 0;
        int? index = findFunctionIndexById(value.functionId);
        if index == () {
            log:printInfo("add_new_fn: add new function '" + value.functionId + "'");
            functions.push({
                id: value.functionId,
                name: value.functionName
            });
            added_fn += 1;
            return {
                message: "Successfully added new function '" + value.functionId + "'"
            };
        } 
        else if index >= 0 {
            Function fn = functions[index];
            int? vers = findFunctionVersionIndexById(fn, value.functionVersion.id);
            if vers == () {
                log:printInfo("add_new_fn: add new function version '" + value.functionVersion.id + "' to function '" + value.functionId + "'");
                fn.versions.push(value.functionVersion);
                return {
                    message: "Successfully added new function version '" + value.functionVersion.id + "' to function '" + value.functionId + "'"
                };
            } else {
                log:printInfo("add_new_fn: failed to add new version '" + value.functionVersion.id + "' as it already exists");
                return {
                    message: "Failed to add new function version '" + value.functionVersion.id + "' to function '" + value.functionId + "'"
                };
            }
        }
        return {
            message: "Failed to add function as specified function ID '" + value.functionId + "' could not be found."
        };

    }

    // Add multiple functions (multiple versions of a function are not allowed)
    remote function add_fns(stream<Function, grpc:Error?> clientStream) returns GenericResponse|error {
        int addCount = 0;
        check clientStream.forEach(function(Function fn) {
            // Make sure the function only has one version
            if (fn.versions.length() > 1) {
                log:printInfo("Cannot add function '" + fn.name + "' because it has more than one version.");
            } else {
                functions.push(fn);
                addCount += 1;
            }
        });
        return {
            message: "Added " + addCount.toString() + " new functions."
        };
    }

    // Delete a particular function
    remote function delete_fn(DeleteFnRequest value) returns GenericResponse|error {
        log:printInfo("delete_fn: Attempting to delete '" + value.functionId + "'");
        // Find the index of the function
        int? functionIndex = findFunctionIndexById(value.functionId);
        if functionIndex != () {
            // If the function was found, delete it
            _ = functions.remove(functionIndex);
            return {
                message: "Successfully removed function with ID: " + value.functionId
            };
        } else {
            // Otherwise, report error
            return error("Cannot delete non-existant function with ID '" + value.functionId + "'.");
        }
    }

    // View a specific version of a function
    remote function show_fn(ShowFnRequest value) returns FunctionVersion|error {
        log:printInfo("show_fn: Searching for version '" + value.functionVersionId + "' in function '" + value.functionId + "'");
        Function fn = check findFunctionById(value.functionId);
        FunctionVersion fnVersion = check findFunctionVersionById(fn, value.functionVersionId);
        log:printInfo("show_fn: Found version '" + value.functionVersionId + "' in function '" + value.functionId + "'");
        return fnVersion;
    }

    // View all versions of a function
    remote function show_all_fns(ShowAllFnsRequest value) returns stream<FunctionVersion, error?>|error {
        log:printInfo("show_all_fns: show all versions of function '" + value.functionId + "'");
        Function fn = check findFunctionById(value.functionId);
        log:printInfo("show_all_fns: found " + fn.versions.length().toString() + " versions of function '" + value.functionId + "'");
        return fn.versions.toStream();
    }

    // View all latest versions of functions implemented in a given language or related to a set of keywords
    remote function show_all_with_criteria(FunctionManagerFunctionQueryResponseCaller caller, stream<FunctionQuery, grpc:Error?> clientStream) returns error? {
        check clientStream.forEach(function(FunctionQuery query) {
            log:printInfo("show_all_with_criteria: Searching for functions with language " + query.language + " and keywords " + query.keywords.toString());
            if query.language is string || query.keywords.length() > 0 {
                Function[] matchingFunctions = [];
                // Note: Don't use a forEach function as that causes a null pointer exception
                foreach Function fn in functions {
                    // Find the latest version of the function that matches
                    // Note that we reverse the versions array to ensure that the first match is the latest
                    foreach FunctionVersion fnVersion in fn.versions.reverse() {
                        boolean languageMatches = false;
                        if query.language is string && query.language.length() > 0 {
                            languageMatches = fnVersion.language.equalsIgnoreCaseAscii(query.language);
                        } else {
                            languageMatches = true;
                        }

                        boolean keywordsMatch = false;
                        if query.keywords.length() > 0 {
                            keywordsMatch = checkKeywordsMatch(fnVersion.keywords, query.keywords);
                        } else {
                            keywordsMatch = true;
                        }

                        boolean versionMatches = languageMatches && keywordsMatch;
                        if versionMatches {
                            // Store the function with only this latest matching version
                            matchingFunctions.push({
                                id: fn.id,
                                name: fn.name,
                                versions: [fnVersion]
                            });
                            // Stop searching through versions since we only need the latest one
                            break;
                        }
                    }
                }

                log:printInfo("show_all_with_criteria: Found " + matchingFunctions.length().toString() + " functions with language '" + query.language + "' and keywords " + query.keywords.toString());
                foreach var fn in matchingFunctions {
                    checkpanic caller->sendFunctionQueryResponse({
                        'function: fn,
                        message: "Found function with language '" + query.language + "' and keywords " + query.keywords.toString()
                    });
                }
                if matchingFunctions.length() == 0 {
                    checkpanic caller->sendFunctionQueryResponse({
                        message: "Failed to find any functions with language '" + query.language + "' and keywords " + query.keywords.toString()
                    });
                }
            } else {
                checkpanic caller->sendFunctionQueryResponse({
                    message: "No language or keyword criteria were provided."
                });
                log:printInfo("No language or keyword criteria were provided.");
            }
        });
        check caller->complete();
    }
}

