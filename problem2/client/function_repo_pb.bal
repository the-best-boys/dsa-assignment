import ballerina/grpc;

public isolated client class FunctionManagerClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(AddNewFnRequest|ContextAddNewFnRequest req) returns (GenericResponse|grpc:Error) {
        map<string|string[]> headers = {};
        AddNewFnRequest message;
        if (req is ContextAddNewFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <GenericResponse>result;
    }

    isolated remote function add_new_fnContext(AddNewFnRequest|ContextAddNewFnRequest req) returns (ContextGenericResponse|grpc:Error) {
        map<string|string[]> headers = {};
        AddNewFnRequest message;
        if (req is ContextAddNewFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <GenericResponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(DeleteFnRequest|ContextDeleteFnRequest req) returns (GenericResponse|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFnRequest message;
        if (req is ContextDeleteFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <GenericResponse>result;
    }

    isolated remote function delete_fnContext(DeleteFnRequest|ContextDeleteFnRequest req) returns (ContextGenericResponse|grpc:Error) {
        map<string|string[]> headers = {};
        DeleteFnRequest message;
        if (req is ContextDeleteFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <GenericResponse>result, headers: respHeaders};
    }

    isolated remote function show_fn(ShowFnRequest|ContextShowFnRequest req) returns (FunctionVersion|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFnRequest message;
        if (req is ContextShowFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FunctionVersion>result;
    }

    isolated remote function show_fnContext(ShowFnRequest|ContextShowFnRequest req) returns (ContextFunctionVersion|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFnRequest message;
        if (req is ContextShowFnRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionManager/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FunctionVersion>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("FunctionManager/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns(ShowAllFnsRequest|ContextShowAllFnsRequest req) returns stream<FunctionVersion, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFnsRequest message;
        if (req is ContextShowAllFnsRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FunctionVersionStream outputStream = new FunctionVersionStream(result);
        return new stream<FunctionVersion, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(ShowAllFnsRequest|ContextShowAllFnsRequest req) returns ContextFunctionVersionStream|grpc:Error {
        map<string|string[]> headers = {};
        ShowAllFnsRequest message;
        if (req is ContextShowAllFnsRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionManager/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FunctionVersionStream outputStream = new FunctionVersionStream(result);
        return {content: new stream<FunctionVersion, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("FunctionManager/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFunction(Function message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFunction(ContextFunction message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveGenericResponse() returns GenericResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <GenericResponse>payload;
        }
    }

    isolated remote function receiveContextGenericResponse() returns ContextGenericResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <GenericResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FunctionVersionStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|FunctionVersion value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|FunctionVersion value;|} nextRecord = {value: <FunctionVersion>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFunctionQuery(FunctionQuery message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFunctionQuery(ContextFunctionQuery message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFunctionQueryResponse() returns FunctionQueryResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <FunctionQueryResponse>payload;
        }
    }

    isolated remote function receiveContextFunctionQueryResponse() returns ContextFunctionQueryResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <FunctionQueryResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionManagerGenericResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendGenericResponse(GenericResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextGenericResponse(ContextGenericResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionManagerFunctionVersionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionVersion(FunctionVersion response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionVersion(ContextFunctionVersion response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionManagerFunctionQueryResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionQueryResponse(FunctionQueryResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionQueryResponse(ContextFunctionQueryResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextFunctionStream record {|
    stream<Function, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunctionQueryStream record {|
    stream<FunctionQuery, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunctionQueryResponseStream record {|
    stream<FunctionQueryResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunctionVersionStream record {|
    stream<FunctionVersion, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunction record {|
    Function content;
    map<string|string[]> headers;
|};

public type ContextFunctionQuery record {|
    FunctionQuery content;
    map<string|string[]> headers;
|};

public type ContextDeleteFnRequest record {|
    DeleteFnRequest content;
    map<string|string[]> headers;
|};

public type ContextFunctionQueryResponse record {|
    FunctionQueryResponse content;
    map<string|string[]> headers;
|};

public type ContextGenericResponse record {|
    GenericResponse content;
    map<string|string[]> headers;
|};

public type ContextShowAllFnsRequest record {|
    ShowAllFnsRequest content;
    map<string|string[]> headers;
|};

public type ContextFunctionVersion record {|
    FunctionVersion content;
    map<string|string[]> headers;
|};

public type ContextShowFnRequest record {|
    ShowFnRequest content;
    map<string|string[]> headers;
|};

public type ContextAddNewFnRequest record {|
    AddNewFnRequest content;
    map<string|string[]> headers;
|};

public type Function record {|
    string id = "";
    string name = "";
    FunctionVersion[] versions = [];
|};

public type FunctionDeveloper record {|
    string name = "";
    string email = "";
|};

public type FunctionQuery record {|
    string language = "";
    string[] keywords = [];
|};

public type DeleteFnRequest record {|
    string functionId = "";
|};

public type FunctionQueryResponse record {|
    Function 'function = {};
    string message = "";
|};

public type GenericResponse record {|
    string message = "";
|};

public type ShowAllFnsRequest record {|
    string functionId = "";
|};

public type FunctionVersion record {|
    string id = "";
    FunctionDeveloper developer = {};
    string language = "";
    string[] keywords = [];
    string contents = "";
|};

public type ShowFnRequest record {|
    string functionId = "";
    string functionVersionId = "";
|};

public type AddNewFnRequest record {|
    FunctionVersion functionVersion = {};
    string functionId = "";
    string functionName = "";
|};

const string ROOT_DESCRIPTOR = "0A1366756E6374696F6E5F7265706F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A1146756E6374696F6E446576656C6F70657212120A046E616D6518012001280952046E616D6512140A05656D61696C1802200128095205656D61696C22A7010A0F46756E6374696F6E56657273696F6E120E0A0269641801200128095202696412300A09646576656C6F70657218022001280B32122E46756E6374696F6E446576656C6F7065725209646576656C6F706572121A0A086C616E677561676518032001280952086C616E6775616765121A0A086B6579776F72647318042003280952086B6579776F726473121A0A08636F6E74656E74731805200128095208636F6E74656E7473225C0A0846756E6374696F6E120E0A0269641801200128095202696412120A046E616D6518022001280952046E616D65122C0A0876657273696F6E7318032003280B32102E46756E6374696F6E56657273696F6E520876657273696F6E732291010A0F4164644E6577466E52657175657374123A0A0F66756E6374696F6E56657273696F6E18012001280B32102E46756E6374696F6E56657273696F6E520F66756E6374696F6E56657273696F6E121E0A0A66756E6374696F6E4964180220012809520A66756E6374696F6E496412220A0C66756E6374696F6E4E616D65180320012809520C66756E6374696F6E4E616D65222B0A0F47656E65726963526573706F6E736512180A076D65737361676518012001280952076D65737361676522310A0F44656C657465466E52657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964225D0A0D53686F77466E52657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964122C0A1166756E6374696F6E56657273696F6E4964180220012809521166756E6374696F6E56657273696F6E496422330A1153686F77416C6C466E7352657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E496422470A0D46756E6374696F6E5175657279121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F72647322580A1546756E6374696F6E5175657279526573706F6E736512250A0866756E6374696F6E18012001280B32092E46756E6374696F6E520866756E6374696F6E12180A076D65737361676518022001280952076D65737361676532C9020A0F46756E6374696F6E4D616E6167657212300A0A6164645F6E65775F666E12102E4164644E6577466E526571756573741A102E47656E65726963526573706F6E736512280A076164645F666E7312092E46756E6374696F6E1A102E47656E65726963526573706F6E73652801122F0A0964656C6574655F666E12102E44656C657465466E526571756573741A102E47656E65726963526573706F6E7365122B0A0773686F775F666E120E2E53686F77466E526571756573741A102E46756E6374696F6E56657273696F6E12360A0C73686F775F616C6C5F666E7312122E53686F77416C6C466E73526571756573741A102E46756E6374696F6E56657273696F6E300112440A1673686F775F616C6C5F776974685F6372697465726961120E2E46756E6374696F6E51756572791A162E46756E6374696F6E5175657279526573706F6E736528013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"function_repo.proto": "0A1366756E6374696F6E5F7265706F2E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223D0A1146756E6374696F6E446576656C6F70657212120A046E616D6518012001280952046E616D6512140A05656D61696C1802200128095205656D61696C22A7010A0F46756E6374696F6E56657273696F6E120E0A0269641801200128095202696412300A09646576656C6F70657218022001280B32122E46756E6374696F6E446576656C6F7065725209646576656C6F706572121A0A086C616E677561676518032001280952086C616E6775616765121A0A086B6579776F72647318042003280952086B6579776F726473121A0A08636F6E74656E74731805200128095208636F6E74656E7473225C0A0846756E6374696F6E120E0A0269641801200128095202696412120A046E616D6518022001280952046E616D65122C0A0876657273696F6E7318032003280B32102E46756E6374696F6E56657273696F6E520876657273696F6E732291010A0F4164644E6577466E52657175657374123A0A0F66756E6374696F6E56657273696F6E18012001280B32102E46756E6374696F6E56657273696F6E520F66756E6374696F6E56657273696F6E121E0A0A66756E6374696F6E4964180220012809520A66756E6374696F6E496412220A0C66756E6374696F6E4E616D65180320012809520C66756E6374696F6E4E616D65222B0A0F47656E65726963526573706F6E736512180A076D65737361676518012001280952076D65737361676522310A0F44656C657465466E52657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964225D0A0D53686F77466E52657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E4964122C0A1166756E6374696F6E56657273696F6E4964180220012809521166756E6374696F6E56657273696F6E496422330A1153686F77416C6C466E7352657175657374121E0A0A66756E6374696F6E4964180120012809520A66756E6374696F6E496422470A0D46756E6374696F6E5175657279121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F72647322580A1546756E6374696F6E5175657279526573706F6E736512250A0866756E6374696F6E18012001280B32092E46756E6374696F6E520866756E6374696F6E12180A076D65737361676518022001280952076D65737361676532C9020A0F46756E6374696F6E4D616E6167657212300A0A6164645F6E65775F666E12102E4164644E6577466E526571756573741A102E47656E65726963526573706F6E736512280A076164645F666E7312092E46756E6374696F6E1A102E47656E65726963526573706F6E73652801122F0A0964656C6574655F666E12102E44656C657465466E526571756573741A102E47656E65726963526573706F6E7365122B0A0773686F775F666E120E2E53686F77466E526571756573741A102E46756E6374696F6E56657273696F6E12360A0C73686F775F616C6C5F666E7312122E53686F77416C6C466E73526571756573741A102E46756E6374696F6E56657273696F6E300112440A1673686F775F616C6C5F776974685F6372697465726961120E2E46756E6374696F6E51756572791A162E46756E6374696F6E5175657279526573706F6E736528013001620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33"};
}

