import ballerina/io;

FunctionManagerClient ep = check new ("http://localhost:9090");

function readQueryResponse(Show_all_with_criteriaStreamingClient streamingClient) returns error? {
    FunctionQueryResponse? result = check streamingClient->receiveFunctionQueryResponse();
    int count = 0;
    while !(result is ()) {
        io:println("show_all_with_criteria result[" + count.toString() + "]:", result);
        result = check streamingClient->receiveFunctionQueryResponse();
        count += 1;
    }
}

public function main() returns error? {

    // add_new_fn
    GenericResponse addNewFnResponse = check ep->add_new_fn({
        functionId: "0",
        functionVersion: 
        {
            id: "4",
            developer: {
                name: "Sarah Jane",
                email: "bluebox@gmail.com"
            },
            language: "C++",
            keywords: ["square", "exponentials"],
            contents: "y = x * x"
        }
    });
    io:println("add_new_fn1: ", addNewFnResponse.message);
    GenericResponse addNewFnResponse2 = check ep->add_new_fn({
        functionId: "6",
        functionName: "Pi"
    });
    io:println("add_new_fn2: ", addNewFnResponse2.message);

    // add_fns
    Add_fnsStreamingClient addFnsClient = check ep->add_fns();
    // We expect the first function to fail to add since it has multiple versions and `add_fns` only allows one version per-function.
    // This can be seen in the server as the log `"Cannot add function \'Add\' because it has more than one version."`
    Function[] functionsToAdd = [
    {
        id: "2",
        name: "Add",
        versions: [
        {
            id: "4",
            developer: {
                name: "Bob the builder",
                email: "nohecant@gmail.com"
            },
            keywords: ["add", "math"],
            language: "python",
            contents: "x + y"
        }, 
                        {
            id: "5",
            developer: {
                name: "Bob the builder",
                email: "nohecant@gmail.com"
            },
            keywords: ["add", "math"],
            language: "python",
            contents: "x + y + z"
        }
        ]
    }, 
    {
        id: "3",
        name: "Subtract",
        versions: []
    }
    ];
    foreach var fn in functionsToAdd {
        check addFnsClient->sendFunction(fn);
    }
    check addFnsClient->complete();
    GenericResponse? addFnsResponse = check addFnsClient->receiveGenericResponse();
    if addFnsResponse != () {
        io:println("add_fns response = ", addFnsResponse.message);
    }

    // delete_fn
    var response = check ep->delete_fn({functionId: "3"});
    io:println("delete_fn: ", response.message);

    // show_fn
    FunctionVersion fnVersion = check ep->show_fn({functionId: "0", functionVersionId: "3"});
    io:println("show_fn: ", fnVersion);

    // show_all_fns
    var showAllFnsClient = check ep->show_all_fns({functionId: "0"});
    check showAllFnsClient.forEach(function(FunctionVersion fnVersion) {
        io:println("show_all_fns: ", fnVersion);
    });

    // show_all_with_criteria
    Show_all_with_criteriaStreamingClient queryClient = check ep->show_all_with_criteria();
    future<error?> f1 = start readQueryResponse(queryClient);
    FunctionQuery[] queries = [
        {language: "JavaScript"}, 
        {language: "JavaScript", keywords: ["math", "square"]}, 
        {keywords: ["math", "square"]}, 
        {keywords: ["math", "circle"]},  // we expect this to fail
        {language: "Python"},  // we expect this to fail
        {language: "C++"}
    ];
    foreach var query in queries {
        check queryClient->sendFunctionQuery(query);
    }
    check queryClient->complete();
    check wait f1;
}

