import ballerina/http;
import ballerina/log;

map<int> scoreToInt = {
    "A+": 100,
    "A": 96,
    "A−": 92,
    "B+": 89,
    "B": 86,
    "B−": 82,
    "C+": 79,
    "C": 76,
    "C−": 72,
    "D+": 69,
    "D": 66,
    "D−": 62,
    "F": 59
};

Student[] students = [
{
    firstname: "John",
    lastname: "Smith",
    username: "john_smith",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
    {
        course: "Data Structures & Algorisms",
        score: "A+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
    {
        course: "Programming 1",
        score: "A+"
    }
    ]
}, 
    {
    firstname: "Lisentu",
    lastname: "Thiswun",
    username: "lisentuthiswun",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "F"
    }
    ]
}, 
{
    firstname: "Ma'am",
    lastname: "Ijöinelate",
    username: "Ijoinlate",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }
    ]
}, 
{
    firstname: "General",
    lastname: "Kenobi",
    username: "boldone22",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
            {
        course: "Data Networks",
        score: "A+"
    }
    ]
}, 
{
    firstname: "Anita",
    lastname: "Cox",
    username: "battleangel",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
    {
        course: "System Application Development",
        score: "C-"
    }
    ]
}, 
{
    firstname: "Buck",
    lastname: "Nekked",
    username: "wintersoldier",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
            {
        course: "Lunch",
        score: "D"
    }
    ]
}, 
{
    firstname: "Jessica",
    lastname: "No",
    username: "notgoingtodothat",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
    {
        course: "Ligression Theory",
        score: "F"
    }
    ]
}, 
{
    firstname: "Dixie",
    lastname: "Normus",
    username: "tiktokstar",
    preferred_formats: ["audio", "video", "text"],
    past_subjects: [
        {
        course: "Data Structures & Algorisms",
        score: "B+"
    }, 
            {
        course: "Programming 2",
        score: "A+"
    }, 
    {
        course: "Engrish for Academic Purposes",
        score: "A"
    }
    ]
}
];

map<Course> courses = {
    "DSA621S": {
        id: "DSA621S",
        name: "Distributed Systems Applications",
        learning_objects: [
        {
            name: "Topic 1",
            description: "",
            difficulty: "Easy",
            mediaType: MEDIA_AUDIO,
            required: true,
            requirements: [
            {
                subject: "Programming 2",
                max_score: "B",
                min_score: "F"
            }
            ]
        }, 
        {
            name: "Topic 2",
            description: "",
            difficulty: "Medium",
            mediaType: MEDIA_AUDIO,
            required: true,
            requirements: [
            {
                subject: "Programming 2",
                max_score: "A+",
                min_score: "C"
            }
            ]
        }, 
        {
            name: "Topic 3",
            description: "",
            difficulty: "Medium",
            mediaType: MEDIA_AUDIO,
            required: false,
            requirements: [
            {
                subject: "Programming 2",
                max_score: "A+",
                min_score: "C"
            }
            ]
        }
        ]
    }
};

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

function findStudent(string username) returns Student? {
    Student? foundStudent = ();
    foreach var student in students {
        if student.username == username {
            foundStudent = student;
            break;
        }
    }
    return foundStudent;
}

function findCourse(string courseId) returns Course? {
    foreach var course in courses {
        if course.id == courseId {
            return course;
        }
    }
    return ();
}

function findPastSubject(Student student, string course) returns PastSubject? {
    foreach PastSubject pastSubject in student.past_subjects {
        if pastSubject?.course == course {
            return pastSubject;
        }
    }
    return ();
}

// Checks if a student meets the requirements of a certain course
function studentMeetsRequirements(Student student, LearningObjectRequirement requirement) returns boolean {
    PastSubject? pastSubject = findPastSubject(student, requirement.subject);
    if !(pastSubject is ()) {
        int? score = scoreToInt[pastSubject.score];
        int? minScore = scoreToInt[requirement.min_score];
        int? maxScore = scoreToInt[requirement.max_score];
        if (!(score is ()) && !(minScore is ()) && !(maxScore is ())) {
            if (score >= minScore && score <= maxScore) {
                return true;
            }
        }
    }
    // If the student didn't have the subject, assume they don't qualify
    return false;
}

function mediaTypeIsPreferred(Student student, MediaType mediaType) returns boolean {
    var preferred_formats = student?.preferred_formats;
    if preferred_formats != () {
        int? idx = preferred_formats.indexOf(mediaType);
        return idx is int;
    }
    return false;
}

service /v1 on ep0 {
    // Creates a learner profile
    resource function post students/create(@http:Payload {} Student payload) returns http:Ok|record {|*http:BadRequest; record {|string message;|} body;|} {
        students.push(payload);
        return <http:Ok>{};
    }

    // Updates a learner profile
    resource function post students/update(string username, @http:Payload {} Student payload) returns http:Ok|http:NotFound {
        if !(payload?.firstname is ()) 
        && !(payload?.lastname is ()) 
        && !(payload?.preferred_formats is ()) {
            Student? foundStudent = findStudent(username);
            // Merge found student with payload
            if !(foundStudent is ()) {
                // Note: We don't allow changing username
                foundStudent.firstname = payload?.firstname ?: "";
                foundStudent.lastname = payload?.lastname ?: "";
                foundStudent.past_subjects = payload.past_subjects;
                foundStudent.preferred_formats = payload?.preferred_formats ?: [];
            } else {
                return <http:NotFound>{body: "Failed to find student with username to update '" + username + "'"};
            }
        }
        return <http:Ok>{};
    }

    // Gets a learner profile's learning materials
    resource function post students/getLearningMaterial(string username, string courseId) returns LearningMaterial|http:NotFound {
        Student? student = findStudent(username);
        Course? course = findCourse(courseId);
        if student is () || course is () {
            if student is () {
                return <http:NotFound>{body: "Failed to find student with username '" + username + "'"};
            }
            if course is () {
                return <http:NotFound>{body: "Failed to find course with ID '" + courseId + "'"};
            }
            // This shouldn't be necessary but Ballerina is weird
            return <http:NotFound>{};
        } else {
            log:printInfo("Getting learning material for course: " + course.name + " for user '" + student.username + "'");
            LearningObject requiredLearningObjects = {
                audio: [],
                text: [],
                video: []
            };
            LearningObject suggestedLearningObjects = {
                audio: [],
                text: [],
                video: []
            };
            foreach var learningObject in course.learning_objects {
                // Only return this learning object if its media type is one that the learner prefers
                boolean isPreferredFormat = mediaTypeIsPreferred(student, learningObject.mediaType);
                if !isPreferredFormat {
                    continue;
                }

                // If the learning object has no requirements, assume they qualify for the course
                boolean requirementsMet = true;
                foreach var requirement in learningObject.requirements {
                    boolean qualifiesForCourse = studentMeetsRequirements(student, requirement);
                    if (!qualifiesForCourse) {
                        requirementsMet = false;
                        break;
                    }
                }
                if (requirementsMet) {
                    if learningObject.required {
                        var mediaArr = requiredLearningObjects[learningObject.mediaType];
                        if (!(mediaArr is ())) {
                            mediaArr.push(learningObject);
                        }
                    } else {
                        var mediaArr = suggestedLearningObjects[learningObject.mediaType];
                        if (!(mediaArr is ())) {
                            mediaArr.push(learningObject);
                        }
                    }
                }
            }
            LearningMaterial mat = {
                course: course.name,
                learning_objects: {
                    required: requiredLearningObjects,
                    suggested: suggestedLearningObjects
                }
            };
            return mat;
        }
    }

    // Lists all learner profiles
    resource function get students/list(int? 'limit, int offset = 0) returns Student[]|record {|*http:BadRequest; record {|string message;|} body;|} {
        Student[] studentsResponse;
        // If a limit is specified, get a subset of the student array
        if 'limit != () {
            int max = offset + 'limit;
            if students.length() < max {
                max = students.length();
            }
            studentsResponse = students.slice(offset, max);
        } else {
            studentsResponse = students;
        }
        return studentsResponse;
    }

    // Gets a particular learner profile by their username
    resource function get students/getUser/[string username]() returns Student|http:NotFound {
        Student? student = findStudent(username);
        if student is () {
            return <http:NotFound>{body: "Failed to find student with username '" + username + "'"};
        } else {
            return student;
        }
    }
}
