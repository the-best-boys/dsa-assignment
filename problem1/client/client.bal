import ballerina/http;
import ballerina/url;
import ballerina/lang.'string;

public type StudentArr Student[];

# a simple api for problem 1
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig = {}, string serviceUrl = "http://localhost:8080/v1") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    #
    # + payload - Learner profile
    # + return - successfully created a new student
    remote isolated function create(Student payload) returns error? {
        string path = string `/students/create`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        _ = check self.clientEp->post(path, request, targetType = http:Response);
    }
    #
    #
    # + username - Username of learner profile to update
    # + payload - New learner profile data
    # + return - successfully updated learner profile
    remote isolated function update(string username, Student payload) returns error? {
        string path = string `/students/update`;
        map<anydata> queryParam = {"username": username};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        _ = check self.clientEp->post(path, request, targetType = http:Response);
    }
    #
    #
    # + username - username of learner profile to get learning material for
    # + courseId - The ID of the course to get learning material for
    # + return - successfully updated learner profile
    remote isolated function getLearningMaterial(string username, string courseId) returns LearningMaterial|error {
        string path = string `/students/getLearningMaterial`;
        map<anydata> queryParam = {"username": username, "courseId": courseId};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        //TODO: Update the request as needed;
        LearningMaterial response = check self.clientEp->post(path, request, targetType = LearningMaterial);
        return response;
    }
    #
    # + 'limit - Limits the number of items on a page
    # + offset - Specifies the page number of the students to be displayed
    # + return - Successfully returned a list of student profiles
    remote isolated function list(int? 'limit = (), int? offset = ()) returns StudentArr|error {
        string path = string `/students/list`;
        map<anydata> queryParam = {"limit": 'limit, "offset": offset};
        path = path + check getPathForQueryParam(queryParam);
        StudentArr response = check self.clientEp->get(path, targetType = StudentArr);
        return response;
    }
    #
    #
    # + username - username of user to get
    # + return - Successfully returned a student
    remote isolated function getUserByusername(string username) returns Student|error {
        string path = string `/students/getUser/${username}`;
        Student response = check self.clientEp->get(path, targetType = Student);
        return response;
    }
}

# Generate query path with query parameter.
#
# + queryParam - Query parameter map
# + return - Returns generated Path or error at failure of client initialization
isolated function getPathForQueryParam(map<anydata> queryParam) returns string|error {
    string[] param = [];
    param[param.length()] = "?";
    foreach var [key, value] in queryParam.entries() {
        if value is () {
            _ = queryParam.remove(key);
        } else {
            if string:startsWith(key, "'") {
                param[param.length()] = string:substring(key, 1, key.length());
            } else {
                param[param.length()] = key;
            }
            param[param.length()] = "=";
            if value is string {
                string updateV = check url:encode(value, "UTF-8");
                param[param.length()] = updateV;
            } else {
                param[param.length()] = value.toString();
            }
            param[param.length()] = "&";
        }
    }
    _ = param.remove(param.length() - 1);
    if param.length() == 1 {
        _ = param.remove(0);
    }
    string restOfPath = string:'join("", ...param);
    return restOfPath;
}
