import ballerina/io;

public function main() returns error? {
    Client vleClient = check new Client();

    string sampleUsername = "john_smith";
    string sampleNewLastName = "Smithson";
    Student newSampleUser = {
        firstname: "River",
        lastname: "Song",
        username: "melodypond",
        past_subjects: [],
        preferred_formats: []
    };

    // ---- List the first 20 learner profiles ----
    Student[] students = check vleClient->list(20, 0);
    io:println("List all students:\n " + students.map((s) => s["firstname"]).toString() + "\n");

    // ---- Get a single learner profile ----
    Student student = check vleClient->getUserByusername(sampleUsername);
    io:println("Full details of user " + sampleUsername + ":\n ", student, "\n");

    // ---- Update learner profile ----
    Student updatedUser = student;
    // Try changing the users last name
    updatedUser["lastname"] = sampleNewLastName;
    io:println("Changing user " + sampleUsername + "'s last name to '" + sampleNewLastName + "' ...");
    check vleClient->update(sampleUsername, updatedUser);
    // Check if the profile has been updated
    student = check vleClient->getUserByusername(sampleUsername);
    io:println("New last name: " + student["lastname"].toString() + "\n");

    // ---- Create a new learner profile ----
    io:println("Creating learner profile: " + newSampleUser["firstname"].toString() + " " + newSampleUser["lastname"].toString() + " (" + newSampleUser["username"] + ") ...");
    check vleClient->create(newSampleUser);
    // Check if the profile was added
    student = check vleClient->getUserByusername(newSampleUser.username);
    io:println("Found created user: '" + newSampleUser["username"] + "'\n");

    // ---- Get learner materials ----
    var learningMaterial = check vleClient->getLearningMaterial(sampleUsername, "DSA621S");
    io:println(learningMaterial);
}
