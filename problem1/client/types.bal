public type LearningObject record {
    LearningObjectMediaArray audio?;
    LearningObjectMediaArray text?;
    LearningObjectMediaArray video?;
};

public type LearningObjectMedia record {
    string name?;
    string description?;
    string difficulty?;
};

public type LearningObjectMediaArray LearningObjectMedia[];

public type PastSubject record {
    string course;
    string score;
};

public type Student record {
    string username;
    string firstname?;
    string lastname?;
    string[] preferred_formats?;
    PastSubject[] past_subjects;
};

public type LearningMaterial record {
    string course?;
    record {LearningObject required; LearningObject suggested;} learning_objects?;
};

public type LearningMaterialRequirement record {
    string course;
    string min_score;
};

public type LearningObjectRequirement record {
    string subject;
    string max_score;
    string min_score;
};

public enum MediaType {
    MEDIA_AUDIO = "audio",
    MEDIA_TEXT = "text",
    MEDIA_VIDEO = "video"
}

public type LearningObjectMediaWithRequirements record {
    *LearningObjectMedia;
    MediaType mediaType;
    LearningObjectRequirement[] requirements;
    boolean required;
};

public type Course record {
    string id;
    string name;
    LearningObjectMediaWithRequirements[] learning_objects;
};
